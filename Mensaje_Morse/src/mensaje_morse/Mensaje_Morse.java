package mensaje_morse;

import java.util.Scanner;

public class Mensaje_Morse {

    static String mensaje;
    static int precio;

    // Imprimir  
    static void print(String indicaciones) {
        System.out.println(indicaciones);
    }
    //Separadores

    static void separador() {
        print("--------------------------------------------------");
    }

    static void separador2() {
        print("");
    }

    public static void main(String[] args) {
        informacion();
        menu();
    }

    //Datos
    static void informacion() {
        print("Universidad Autonoma de Campeche.");
        print("Facultad de Ingenieria");
        print("Ingenieria en Sistemas Computacionales");
        print("Lenguaje de Programacion 2 - B");
        print("Rodrigo M Gaytan Balan - 63842");
        print("Daniel A Colli Aguilar - 63868");
    }

    //Menu
    static void menu() {
        separador();
        Scanner teclado = new Scanner(System.in);
        print("Bienvenido(a) a nuestro sistema de mensajeria.");
        separador2();
        print("--1.Enviar Mensaje");
        separador2();
        print("--2.Salir");
        separador2();
        print("Ingrese el numero de la opcion que desea: ");
        int menu1 = teclado.nextInt();
        menu2(menu1);
        separador();
    }
    //Menu2

    static void menu2(int menu1) {
        if (menu1 == 1) {
            //Ingresar un mensaje que se guarda en la variable mensaje
            Scanner teclado = new Scanner(System.in);
            separador2();
            separador();
            print("Ingrese su mensaje: ");
            mensaje = teclado.nextLine();
            String frase = mensaje;
            String[] fraseArr = frase.split(" ");

            separador();
            print("Mensaje Introducido: " + mensaje);
            separador2();
            print("Mensaje en Morse: ");
            char[] morse = mensaje.toCharArray(); //Se usa un Array para guardar varios datos en una variable

            for (int i = 0; i < mensaje.length(); i++) { //Con un for y con el .length contamos las letras
                switch (morse[i]) { //Con el swith para dependiendo de cada letra de la frase imprime el codigo morse
                    case 'a':
                    case 'A':
                        System.out.print(".- ");
                        break;
                    case 'b':
                    case 'B':
                        System.out.print("-... ");
                        break;
                    case 'c':
                    case 'C':
                        System.out.print("-.-.");
                        break;
                    case 'd':
                    case 'D':
                        System.out.print("-..");
                        break;
                    case 'e':
                    case 'E':
                        System.out.print(". ");
                        break;
                    case 'f':
                    case 'F':
                        System.out.print("..-. ");
                        break;
                    case 'g':
                    case 'G':
                        System.out.print("--. ");
                        break;
                    case 'h':
                    case 'H':
                        System.out.print(".... ");
                        break;
                    case 'i':
                    case 'I':
                        System.out.print(".. ");
                        break;
                    case 'j':
                    case 'J':
                        System.out.print(".--- ");
                        break;
                    case 'k':
                    case 'K':
                        System.out.print("-.-. ");
                        break;
                    case 'l':
                    case 'L':
                        System.out.print(".-.. ");
                        break;
                    case 'm':
                    case 'M':
                        System.out.print("-- ");
                        break;
                    case 'n':
                    case 'N':
                        System.out.print("-. ");
                        break;
                    case 'ñ':
                    case 'Ñ':
                        System.out.print("|| ");
                        break;
                    case 'o':
                    case 'O':
                        System.out.print("--- ");
                        break;
                    case 'p':
                    case 'P':
                        System.out.print(".--. ");
                        break;
                    case 'q':
                    case 'Q':
                        System.out.print("--.- ");
                        break;
                    case 'r':
                    case 'R':
                        System.out.print(".-. ");
                        break;
                    case 's':
                    case 'S':
                        System.out.print("... ");
                        break;
                    case 't':
                    case 'T':
                        System.out.print("- ");
                        break;
                    case 'u':
                    case 'U':
                        System.out.print("..- ");
                        break;
                    case 'v':
                    case 'V':
                        System.out.print("...- ");
                        break;
                    case 'w':
                    case 'W':
                        System.out.print(".-- ");
                        break;
                    case 'x':
                    case 'X':
                        System.out.print("-..- ");
                        break;
                    case 'y':
                    case 'Y':
                        System.out.print("-.-- ");
                        break;
                    case 'z':
                    case 'Z':
                        System.out.print("--.. ");
                        break;
                    case '.':
                        System.out.print(".-.-.-");
                        break;
                    case ',':
                        System.out.print("--..--");
                        break;
                    case ':':
                        System.out.print("---...");
                        break;
                    case '-':
                        System.out.print("-...-");
                        break;
                    case '=':
                        System.out.print("-...-");
                        break;
                    case ';':
                        System.out.print("-.-.-.");
                        break;
                    case '?':
                        System.out.print("..--..");
                        break;
                    case '"':
                        System.out.print(".-..-.");
                        break;
                    case '/':
                        System.out.print("-..-.");
                        break;
                    case '1':
                        System.out.print(".----");
                        break;
                    case '2':
                        System.out.print("..---");
                        break;
                    case '3':
                        System.out.print("...--");
                        break;
                    case '4':
                        System.out.print("....-");
                        break;
                    case '5':
                        System.out.print(".....");
                        break;
                    case '6':
                        System.out.print("-....");
                        break;
                    case '7':
                        System.out.print("--...");
                        break;
                    case '8':
                        System.out.print("---..");
                        break;
                    case '9':
                        System.out.print("----.");
                        break;
                    case '0':
                        System.out.print("-----");
                        break;
                    default:
                        System.out.print(" / ");

                }

            }

            for (String cadaPalabra : fraseArr) {//Aqui tienes cada palabra individual de cadaPalabra
                int numeroLetras = cadaPalabra.length();//Cuenta las letras de cada palabra
                //Con un If para ir sumando el precio de cada palabra
                if (numeroLetras <= 3) {
                    precio = precio + 10;
                } else {
                    precio = precio + 15;
                }

            }
            pago();

        }

    }

    static void pago() {
        separador2();
        separador();
        System.out.println("El precio del mensaje es de $" + precio);
        Scanner lector = new Scanner(System.in);
        print("¿Desea proceder al pago?");
        separador2();
        print("Si -> 1");
        print("No (Salir) -> 2");
        separador2();
        print("Ingrese el numero de la opcion que desea: ");
        int pago1 = lector.nextInt();

        if (pago1 == 1) {
            separador2();
            Scanner nnimporte = new Scanner(System.in);
            print("¿Con que importes desea pagar? (Solo se aceptan importes de 100, 200, 500 y 1000 pesos");
            int importe = lector.nextInt();
            if (importe == 100) {
                Scanner Nimporte = new Scanner(System.in);
                print("¿Cuantos importes de 100 pesos desea hacer?");
                int nimporte = lector.nextInt();
                int Total = nimporte * importe;
                if (Total < precio) {
                    separador();
                    print("El monto ingresado no es suficiente");
                    pago();
                } else {
                    int cambio = Total - precio;
                    separador();
                    System.out.print("Su cambio es de " + cambio + " pesos");
                    separador2();
                    print("Gracias por usar nuestro servicio de mensajeria.");
                    print("¡Vuelva pronto!");
                }

            }
            if (importe == 200) {
                Scanner Nimporte = new Scanner(System.in);
                print("¿Cuantos importes de 200 pesos desea hacer?");
                int nimporte = lector.nextInt();
                int Total = nimporte * importe;
                if (Total < precio) {
                    separador();
                    print("El monto ingresado no es suficiente");
                    pago();
                } else {
                    int cambio = Total - precio;
                    separador();
                    System.out.print("Su cambio es de " + cambio + " pesos");
                    separador2();
                    print("Gracias por usar nuestro servicio de mensajeria.");
                    print("¡Vuelva pronto!");
                }

            }
            if (importe == 500) {
                Scanner Nimporte = new Scanner(System.in);
                print("¿Cuantos importes de 500 pesos desea hacer?");
                int nimporte = lector.nextInt();
                int Total = nimporte * importe;
                if (Total < precio) {
                    separador();
                    print("El monto ingresado no es suficiente");
                    pago();
                } else {
                    int cambio = Total - precio;
                    separador();
                    System.out.print("Su cambio es de " + cambio + " pesos");
                    separador2();
                    print("Gracias por usar nuestro servicio de mensajeria.");
                    print("¡Vuelva pronto!");
                }

            }
            if (importe == 1000) {
                Scanner Nimporte = new Scanner(System.in);
                print("¿Cuantos importes de 1000 pesos desea hacer?");
                int nimporte = lector.nextInt();
                int Total = nimporte * importe;
                if (Total < precio) {
                    separador();
                    print("El monto ingresado no es suficiente");
                    pago();
                } else {
                    int cambio = Total - precio;
                    separador();
                    System.out.print("Su cambio es de " + cambio + " pesos");
                    separador2();
                    print("Gracias por usar nuestro servicio de mensajeria.");
                    print("¡Vuelva pronto!");
                }

            }

            if (importe < 100 || importe > 1000 || importe == 300 || importe == 400) {
                separador();
                print("Importe no permitido");
                pago();
            }
        }

    }

}
